# 3의 배수일때 fizz
# 5의 배수일때 buzz
# 15의 배수면 fizzbuzz

for j in range(1, 17):
    if j % 15 == 0:
        print("fizzbuzz")
    elif j % 3 == 0:
        print("fizz")
    elif j % 5 == 0:
        print("buzz")
    else:
        print(j)
